package com.example.ensiklopediahewan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_karnivora.*

class activity_karnivora : AppCompatActivity() {

    private val list = ArrayList<Karnivora>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_karnivora)

        rv_karnivora.setHasFixedSize(true)

        list.addAll(getListKarnivora())
        showRecyclerList()
    }

    fun getListKarnivora(): ArrayList<Karnivora> {
        val dataName = resources.getStringArray(R.array.data_name_karnivora)
        val dataDescription = resources.getStringArray(R.array.data_description_karnivora)
        val dataPhoto = resources.getStringArray(R.array.data_photo_karnivora)
        val listKarnivora = ArrayList<Karnivora>()
        for (position in dataName.indices) {
            val karnivora = Karnivora(
                dataName[position],
                dataDescription[position],
                dataPhoto[position]
            )
            listKarnivora.add(karnivora)
        }
        return listKarnivora
    }

    private fun showRecyclerList() {
        rv_karnivora.layoutManager = LinearLayoutManager(this)
        val listKarnivoraAdapter = ListKarnivoraAdapter(list)
        rv_karnivora.adapter = listKarnivoraAdapter
    }
}