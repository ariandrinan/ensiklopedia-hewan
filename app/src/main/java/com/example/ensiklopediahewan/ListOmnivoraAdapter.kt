package com.example.ensiklopediahewan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_row_omnivora.view.*

class ListOmnivoraAdapter(private val listOmnivora: ArrayList<Omnivora>) : RecyclerView.Adapter<ListOmnivoraAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_row_omnivora, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(listOmnivora[position])
    }

    override fun getItemCount(): Int = listOmnivora.size

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(omnivora: Omnivora) {
            with(itemView){
                Glide.with(itemView.context)
                    .load(omnivora.photo)
                    .apply(RequestOptions().override(55, 55))
                    .into(img_item_photo)
                tv_item_name.text = omnivora.name
                tv_item_description.text = omnivora.description
            }
        }
    }
}